const { MongoClient } = require('mongodb');

class Database {
    constructor() {
        this.dbName = 'mydatabase';
        this.uri = 'mongodb+srv://valentinribezzi:FZtWbihUEj9Pj2EA@challengestackreact.abto0il.mongodb.net/';
        this.client = new MongoClient(this.uri, { useNewUrlParser: true, useUnifiedTopology: true });
    }

    async connect() {
        if (!this.isConnected()) {
            try {
                await this.client.connect();
                console.log('Connected to MongoDB');
            } catch (err) {
                console.error('Error connecting to MongoDB', err);
            }
        }
    }
    isConnected() {
        return this.client.isConnected();
    }
    async disconnect() {
        if (this.isConnected()) {
            await this.client.close();
            console.log('Disconnected from MongoDB');
        }
    }
    getDatabase() {
        return this.client.db(this.dbName);
    }
}

const instance = new Database();

module.exports = instance;