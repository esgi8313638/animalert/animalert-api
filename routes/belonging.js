// Desc: Belonging API
// Date: 12 Février 2024

// Importation des modules
const express = require('express');
const app = express();
const objectId = require('mongodb').ObjectId;
const bodyParser = require('body-parser');

// Importation de la base de données
const dbInstance = require('../database/connect');
const db = dbInstance.getDatabase();
const belongingCollection = db.collection('Belonging');

// Importation et mise en place du CORS pour les requêtes
const cors = require('cors');
app.use(cors());


/**
 * @swagger
 * '/api/belonging':
 *  get:
 *      tags:
 *          - Belonging
 *      summary: Récupérer les liaisons entre utilisateurs et animaux
 *      responses:
 *          '200':
 *              description: Succès
 *          '500':
 *              description: Erreur serveur
 */
app.get('/api/belongings', async (req, res) => {
    try {
        const data = await belongingCollection.find().toArray();
        res.json(data);
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

/**
 * @swagger
 * '/api/belonging/{id}':
 *  get:
 *      tags:
 *          - Belonging
 *      summary: Récupérer les liaisons entre un utilisateur et ses animaux
 *      parameters:
 *          - name: id
 *            in: path
 *            required: true
 *            description: ID de la réponse d'alerte à récupérer
 *            schema:
 *              type: string
 *      responses:
 *          '200':
 *              description: Succès
 *          '404':
 *              description: Réponse d'alerte non trouvée
 *          '500':
 *              description: Erreur serveur
 */
app.get('/api/belonging/:id', async (req, res) => {
    try {
        var id = new objectId(req.params.id);
        const data = await belongingCollection.findOne({ userID: id });
        res.json(data);
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

/**
 * @swagger
 * '/api/belonging':
 *  post:
 *      tags:
 *          - Belonging
 *      summary: Ajouter une nouvelle liaison, utilisateur-animal
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      properties:
 *                          userID:
 *                              type: number
 *                          animalID:
 *                              type: number
 *                          dateStatement:
 *                              type: datetime
 *      responses:
 *          '201':
 *              description: Liaison ajoutée avec succès
 *          '400':
 *              description: Requête invalide
 *          '500':
 *              description: Erreur serveur
 */
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.post('/api/belongings', async (req, res) => {
    const newData = req.body;
    try {
        // Si la date est fournie dans le corps de la requête, la convertir en objet Date
        if (newData.dateStatement) {
            newData.dateStatement = new Date(newData.dateStatement);
        }

        newData._id = new objectId(); // Générer un nouvel ObjectId et l'assigner à l'objet newData
        const result = await belongingCollection.insertOne(newData);
        res.status(201).json({ insertedId: newData._id }); // Envoyer l'ID inséré en réponse
    } catch (err) {
        res.status(400).json({ message: err.message });
    }
});

/**
 * @swagger
 * '/api/belonging/{id}':
 *  put:
 *      tags:
 *          - Belonging
 *      summary: Mettre à jour une une liaison utilisateur-animal
 *      parameters:
 *          - name: id
 *            in: path
 *            required: true
 *            description: ID de la réponse la liaison à mettre à jour
 *            schema:
 *              type: string
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      properties:
 *                          userID:
 *                              type: number  
 *                          animalID:
 *                              type: number
 *                          dateStatement:
 *                              type: datetime
 *      responses:
 *          '200':
 *              description: Réponse de la liaison mise à jour avec succès
 *          '400':
 *              description: Requête invalide
 *          '500':
 *              description: Erreur serveur
 */
app.put('/api/belongings/:id', async (req, res) => {
    const newData = req.body;
    try {
        // Si la date est fournie dans le corps de la requête, la convertir en objet Date
        if (newData.dateStatement) {
            newData.dateStatement = new Date(newData.dateStatement);
        }
        const result = await belongingCollection.updateOne(
            { _id: new objectId(req.params.id) },
            { $set: newData }
        );
        res.json(result);
    } catch (err) {
        res.status(400).json({ message: err.message });
    }
});


/**
 * @swagger
 * '/api/belonging/{id}':
 *  delete:
 *      tags:
 *          - Belonging
 *      summary: Supprimer une réponse d'alerte par son ID
 *      parameters:
 *          - name: id
 *            in: path
 *            required: true
 *            description: ID de la liaison à supprimer
 *            schema:
 *              type: string
 *      responses:
 *          '200':
 *              description: Réponse d'alerte supprimée avec succès
 *          '404':
 *              description: Réponse d'alerte non trouvée
 *          '500':
 *              description: Erreur serveur
 */
app.delete('/api/belonging/:id', async (req, res) => {
    try {
        const id = new objectId(req.params.id);
        const result = await belongingCollection.deleteOne({ _id: id });
        if (result.deletedCount === 0) {
            return res.status(404).json({ message: "Aucune appartenance avec cet ID n'a été trouvée." });
        }
        res.json({ message: "L'appartenance de cet animal à un utilisateur a été supprimée." });
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

module.exports = app;