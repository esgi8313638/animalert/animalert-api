// Desc: Alerts API
// Date: 12 Février 2024

// Importation des modules
const express = require('express');
const app = express();
const objectId = require('mongodb').ObjectId;
const bodyParser = require('body-parser');

// Importation de la base de données
const dbInstance = require('../database/connect');
const db = dbInstance.getDatabase();
const alertsCollection = db.collection('Alerts');

// Importation et mise en place du CORS pour les requêtes
const cors = require('cors');
app.use(cors());


/**
 * @swagger
 * '/api/alerts':
 *  get:
 *      tags:
 *          - Alerts
 *      summary: Récupérer toutes les alertes
 *      responses:
 *          '200':
 *              description: Succès
 *          '500':
 *              description: Erreur serveur
 */
app.get('/api/alerts', async (req, res) => {
    try {
        const data = await alertsCollection.find().toArray();
        res.json(data);
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

/**
 * @swagger
 * '/api/alerts/{id}':
 *  get:
 *      tags:
 *          - Alerts
 *      summary: Récupérer une alerte par son ID
 *      parameters:
 *          - name: id
 *            in: path
 *            required: true
 *            description: ID de l'alerte à récupérer
 *            schema:
 *              type: string
 *      responses:
 *          '200':
 *              description: Succès
 *          '404':
 *              description: Alerte non trouvée
 *          '500':
 *              description: Erreur serveur
 */
app.get('/api/alerts/:id', async (req, res) => {
    try {
        var id = new objectId(req.params.id);
        const data = await alertsCollection.findOne({ _id: id });
        res.json(data);
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

/**
 * @swagger
 * '/api/alerts':
 *  post:
 *      tags:
 *          - Alerts
 *      summary: Ajouter une nouvelle alerte
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      properties:
 *                          userID:
 *                              type: number
 *                          location:
 *                              type: string
 *                          resolved:
 *                              type: boolean
 *                          animalID:
 *                              type: number    
 *      responses:
 *          '201':
 *              description: Alerte ajoutée avec succès
 *          '400':
 *              description: Requête invalide
 *          '500':
 *              description: Erreur serveur
 */
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.post('/api/alerts', async (req, res) => {
    const newData = req.body;
    try {
        newData._id = new objectId(); // Générer un nouvel ObjectId et l'assigner à l'objet newData
        const result = await alertsCollection.insertOne(newData);
        const alert = await alertsCollection.findOne({ _id: newData._id });
        res.status(201).json({ alert: alert }); // Envoyer l'ID inséré en réponse
    } catch (err) {
        res.status(400).json({ message: err.message });
    }
});

/**
 * @swagger
 * '/api/alerts/{id}':
 *  put:
 *      tags:
 *          - Alerts
 *      summary: Mettre à jour une alerte par son ID
 *      parameters:
 *          - name: id
 *            in: path
 *            required: true
 *            description: ID de l'alerte à mettre à jour
 *            schema:
 *              type: string
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      properties:
 *                          alertID:
 *                              type: number
 *                          userID:
 *                              type: number
 *                          location:
 *                              type: string
 *                          resolved:
 *                              type: boolean
 *                          animalID:
 *                              type: number
 *      responses:
 *          '200':
 *              description: Alerte mise à jour avec succès
 *          '400':
 *              description: Requête invalide
 *          '500':
 *              description: Erreur serveur
 */
app.put('/api/alerts/:id', async (req, res) => {
    const newData = req.body;
    try {
        const result = await alertsCollection.updateOne(
            { _id: new objectId(req.params.id) },
            { $set: newData }
        );
        res.json(result);
    } catch (err) {
        res.status(400).json({ message: err.message });
    }
});


/**
 * @swagger
 * '/api/alerts/{id}':
 *  delete:
 *      tags:
 *          - Alerts
 *      summary: Supprimer une alerte par son ID
 *      parameters:
 *          - name: id
 *            in: path
 *            required: true
 *            description: ID de l'alerte à supprimer
 *            schema:
 *              type: string
 *      responses:
 *          '200':
 *              description: Alerte supprimée avec succès
 *          '404':
 *              description: Alerte non trouvée
 *          '500':
 *              description: Erreur serveur
 */
app.delete('/api/alerts/:id', async (req, res) => {
    try {
        const id = new objectId(req.params.id);
        const result = await alertsCollection.deleteOne({ _id: id });
        if (result.deletedCount === 0) {
            return res.status(404).json({ message: "Aucune alerte avec cet ID n'a été trouvée." });
        }
        res.json({ message: "L'alerte a été supprimée." });
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

module.exports = app;