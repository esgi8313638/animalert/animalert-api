// Desc: Alerts Answers Pictures API
// Date: 12 Février 2024

// Importation des modules
const express = require('express');
const app = express();
const objectId = require('mongodb').ObjectId;
const bodyParser = require('body-parser');

// Importation de la base de données
const dbInstance = require('../database/connect');
const db = dbInstance.getDatabase();
const alertsAnswersPicturesCollection = db.collection('AlertsAnswersPictures');

// Importation et mise en place du CORS pour les requêtes
const cors = require('cors');
app.use(cors());


/**
 * @swagger
 * '/api/alerts-answers-pictures':
 *  get:
 *      tags:
 *          - Alerts Answers Pictures
 *      summary: Récupérer toutes les images des alertes
 *      responses:
 *          '200':
 *              description: Succès
 *          '500':
 *              description: Erreur serveur
 */
app.get('/api/alerts-answers-pictures', async (req, res) => {
    try {
        const data = await alertsAnswersPicturesCollection.find().toArray();
        res.json(data);
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

/**
 * @swagger
 * '/api/alerts-answers-pictures/{id}':
 *  get:
 *      tags:
 *          - Alerts Answers Pictures
 *      summary: Récupérer l'images d'alerte par son ID
 *      parameters:
 *          - name: id
 *            in: path
 *            required: true
 *            description: ID de la réponse de l'image de l'alerte
 *            schema:
 *              type: string
 *      responses:
 *          '200':
 *              description: Succès
 *          '404':
 *              description: Réponse d'alerte non trouvée
 *          '500':
 *              description: Erreur serveur
 */
app.get('/api/alerts-answers-pictures/:id', async (req, res) => {
    try {
        var id = new objectId(req.params.id);
        const data = await alertsAnswersPicturesCollection.findOne({ _id: id });
        res.json(data);
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

/**
 * @swagger
 * '/api/alerts-answers-pictures':
 *  post:
 *      tags:
 *          - Alerts Answers Pictures
 *      summary: Ajouter une nouvelle réponse d'alerte
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      properties:
 *                          alertsanswersID:
 *                              type: number
 *                          pictureID:
 *                              type: number
 *      responses:
 *          '201':
 *              description: Réponse d'image d'alerte ajoutée avec succès
 *          '400':
 *              description: Requête invalide
 *          '500':
 *              description: Erreur serveur
 */
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.post('/api/alerts-answers-pictures', async (req, res) => {
    const newData = req.body;
    try {
        newData._id = new objectId(); // Générer un nouvel ObjectId et l'assigner à l'objet newData
        const result = await alertsAnswersPicturesCollection.insertOne(newData);
        res.status(201).json({ insertedId: newData._id }); // Envoyer l'ID inséré en réponse
    } catch (err) {
        res.status(400).json({ message: err.message });
    }
});

/**
 * @swagger
 * '/api/alerts-answers-pictures/{id}':
 *  delete:
 *      tags:
 *          - Alerts Answers Pictures
 *      summary: Supprimer une réponse d'alerte par son ID
 *      parameters:
 *          - name: id
 *            in: path
 *            required: true
 *            description: ID de la réponse d'alerte à supprimer
 *            schema:
 *              type: string
 *      responses:
 *          '200':
 *              description: Réponse d'alerte supprimée avec succès
 *          '404':
 *              description: Réponse d'alerte non trouvée
 *          '500':
 *              description: Erreur serveur
 */
app.delete('/api/alerts-answers-pictures/:id', async (req, res) => {
    try {
        const id = new objectId(req.params.id);
        const result = await alertsAnswersPicturesCollection.deleteOne({ _id: id });
        if (result.deletedCount === 0) {
            return res.status(404).json({ message: "Aucune réponse de photo de réponse pour une alerte avec cet ID n'a été trouvée." });
        }
        res.json({ message: "La photo de la réponse de l'alerte a été supprimée." });
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

module.exports = app;