// Desc: Animals API
// Date: 12 Février 2024

// Importation des modules
const express = require('express');
const app = express();
const objectId = require('mongodb').ObjectId;
const bodyParser = require('body-parser');
const Double = require('mongodb').Double;

// Importation de la base de données
const dbInstance = require('../database/connect');
const db = dbInstance.getDatabase();
const animalsCollection = db.collection('Animals');

// Importation et mise en place du CORS pour les requêtes
const cors = require('cors');
app.use(cors());


/**
 * @swagger
 * '/api/animals':
 *  get:
 *      tags:
 *          - Animals
 *      summary: Récupérer tous les animaux
 *      responses:
 *          '200':
 *              description: Succès
 *          '500':
 *              description: Erreur serveur
 */
app.get('/api/animals', async (req, res) => {
    try {
        const data = await animalsCollection.find().toArray();
        res.json(data);
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});


/**
 * @swagger
 * '/api/animals/{id}':
 *  get:
 *      tags:
 *          - Animals
 *      summary: Récupérer un animal par son ID
 *      parameters:
 *          - name: id
 *            in: path
 *            required: true
 *            description: ID de l'animal à récupérer
 *            schema:
 *              type: string
 *      responses:
 *          '200':
 *              description: Succès
 *          '404':
 *              description: Animal non trouvé
 *          '500':
 *              description: Erreur serveur
 */
app.get('/api/animals/:id', async (req, res) => {
    try {
        var id = new objectId(req.params.id);
        const data = await animalsCollection.findOne({ _id: id });
        res.json(data);
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

// GET by user ID
app.get('/api/animals/user/:id', async (req, res) => {
    try {
        var id = new objectId(req.params.id);
        const data = await animalsCollection.find({ userId: id }).toArray();
        res.json(data);
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});


/**
 * @swagger
 * '/api/animals':
 *  post:
 *      tags:
 *          - Animals
 *      summary: Ajouter un nouvel animal
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      properties:
 *                          animalID:
 *                              type: number
 *                          name:
 *                              type: string
 *                          breed:
 *                              type: string
 *                          puceID:
 *                              type: string
 *                          tattoo:
 *                              type: string
 *                          color:
 *                              type: string
 *                          fur:
 *                              type: string
 *                          height:
 *                              type: number
 *                          weight:
 *                              type: number
 *                          description:
 *                              type: string
 *                          pictures:
 *                              type: number
 *                      example:
 *                          animalID: 2
 *                          name: "Francisse"
 *                          breed: "Gros chien qui mate des culs"
 *                          puceID: "1234567890"
 *                          tattoo: "T123"
 *                          color: "Black (Raciste ?)"
 *                          fur: "Pas tres grand ni tres muscle"
 *                          height: 60.5
 *                          weight: 30.2
 *                          description: "Toutou à valentin"
 *                          pictures: 1
 *      responses:
 *          '201':
 *              description: Animal ajoutée avec succès
 *          '400':
 *              description: Requête invalide
 *          '500':
 *              description: Erreur serveur
 */
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.post('/api/animals', async (req, res) => {
    let newData = req.body;
    newData.height = new Double(newData.height);
    newData.weight = new Double(newData.weight);
    newData.userId = new objectId(newData.userId);
    newData.pictures = new Buffer(newData.pictures.split(","[1]), 'base64');
    try {
        newData._id = new objectId(); // Générer un nouvel ObjectId et l'assigner à l'objet newData
        const result = await animalsCollection.insertOne(newData);
        const animal = await animalsCollection.findOne({ _id: newData._id });
        res.status(201).json({ animal: animal }); // Envoyer l'ID inséré en réponse
    } catch (err) {
        res.status(400).json({ message: err });
    }
});

/**
 * @swagger
 * '/api/animals/{id}':
 *  put:
 *      tags:
 *          - Animals
 *      summary: Mettre à jour un animal par son ID
 *      parameters:
 *          - name: id
 *            in: path
 *            required: true
 *            description: ID de l'animal à mettre à jour
 *            schema:
 *              type: string
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      properties:
 *                          animalID:
 *                              type: number
 *                          name:
 *                              type: string
 *                          breed:
 *                              type: string
 *                          puceID:
 *                              type: string
 *                          tattoo:
 *                              type: string
 *                          color:
 *                              type: string
 *                          fur:
 *                              type: string
 *                          height:
 *                              type: number
 *                          weight:
 *                              type: number
 *                          description:
 *                              type: string
 *                          pictures:
 *                              type: number
 *                      example:
 *                          animalID: 2
 *                          name: "Francisse"
 *                          breed: "Gros chien qui mate des culs"
 *                          puceID: "1234567890"
 *                          tattoo: "T123"
 *                          color: "Black (Raciste ?)"
 *                          fur: "Pas tres grand ni tres muscle"
 *                          height: 60.5
 *                          weight: 30.2
 *                          description: "Toutou à valentin"
 *                          pictures: 1
 *      responses:
 *          '200':
 *              description: Animal mis à jour avec succès
 *          '400':
 *              description: Requête invalide
 *          '500':
 *              description: Erreur serveur
 */
app.put('/api/animals/:id', async (req, res) => {
    const newData = req.body;
    try {
        const result = await animalsCollection.updateOne(
            { _id: new objectId(req.params.id) },
            { $set: newData }
        );
        res.json(result);
    } catch (err) {
        res.status(400).json({ message: err.message });
    }
});


/**
 * @swagger
 * '/api/animals/{id}':
 *  delete:
 *      tags:
 *          - Animals
 *      summary: Supprimer un animal par son ID
 *      parameters:
 *          - name: id
 *            in: path
 *            required: true
 *            description: ID de l'animal à supprimer
 *            schema:
 *              type: string
 *      responses:
 *          '200':
 *              description: Animal supprimé avec succès
 *          '404':
 *              description: Animal non trouvé
 *          '500':
 *              description: Erreur serveur
 */
app.delete('/api/animals/:id', async (req, res) => {
    try {
        const id = new objectId(req.params.id);
        const result = await animalsCollection.deleteOne({ _id: id });
        if (result.deletedCount === 0) {
            return res.status(404).json({ message: "Aucune animal  avec cet ID n'a été trouvée." });
        }
        res.json({ message: "L'animal a été supprimé." });
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

module.exports = app;