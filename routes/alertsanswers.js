// Desc: Alerts Answers API
// Date: 12 Février 2024

// Importation des modules
const express = require('express');
const app = express();
const objectId = require('mongodb').ObjectId;
const bodyParser = require('body-parser');

// Importation de la base de données
const dbInstance = require('../database/connect');
const db = dbInstance.getDatabase();
const alertsAnswersCollection = db.collection('AlertsAnswers');

// Importation et mise en place du CORS pour les requêtes
const cors = require('cors');
app.use(cors());


/**
 * @swagger
 * '/api/alerts-answers':
 *  get:
 *      tags:
 *          - Alerts Answers
 *      summary: Récupérer toutes les réponses d'alertes
 *      responses:
 *          '200':
 *              description: Succès
 *          '500':
 *              description: Erreur serveur
 */
app.get('/api/alerts-answers', async (req, res) => {
    try {
        const data = await alertsAnswersCollection.find().toArray();
        res.json(data);
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

/**
 * @swagger
 * '/api/alerts-answers/{id}':
 *  get:
 *      tags:
 *          - Alerts Answers
 *      summary: Récupérer une réponse d'alerte par son ID
 *      parameters:
 *          - name: id
 *            in: path
 *            required: true
 *            description: ID de la réponse d'alerte à récupérer
 *            schema:
 *              type: string
 *      responses:
 *          '200':
 *              description: Succès
 *          '404':
 *              description: Réponse d'alerte non trouvée
 *          '500':
 *              description: Erreur serveur
 */
app.get('/api/alerts-answers/:id', async (req, res) => {
    try {
        var id = new objectId(req.params.id);
        const data = await alertsAnswersCollection.findOne({ _id: id });
        res.json(data);
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

/**
 * @swagger
 * '/api/alerts-answers/user/{id}':
 *  get:
 *      tags:
 *          - Alerts Answers
 *      summary: Récupérer le(s) réponse(s) d'alerte(s) pour un utilisateur par son ID
 *      parameters:
 *          - name: id
 *            in: path
 *            required: true
 *            description: ID de l'utilisateur pour récupérer le(s) réponse(s) d'alerte(s) qui le concerne
 *            schema:
 *              type: string
 *      responses:
 *          '200':
 *              description: Succès
 *          '404':
 *              description: Réponse(s) d'alerte(s) non trouvée(s)
 *          '500':
 *              description: Erreur serveur
 */
app.get('/api/alerts-answers/user/:id', async (req, res) => {
    try {
        var id = req.params.id;
        const data = await alertsAnswersCollection.find({ userId: id }).toArray();
        res.json(data);
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

/**
 * @swagger
 * '/api/alerts-answers/animals/user/{id}':
 *  get:
 *      tags:
 *          - Alerts Answers
 *      summary: Récupérer le ou les animaux concernés par une alerte pour un utilisateur par son ID
 *      parameters:
 *          - name: id
 *            in: path
 *            required: true
 *            description: ID de l'utilisateur pour récupérer le ou les animaux concernés par une alerte qui le concerne
 *            schema:
 *              type: string
 *      responses:
 *          '200':
 *              description: Succès
 *          '404':
 *              description: Animaux non trouvés
 *          '500':
 *              description: Erreur serveur
 */
// app.get('/api/alerts-answers/animals/user/:id', async (req, res) => {
//     try {
//         var id = req.params.id;
//         const data = await alertsAnswersCollection.find({ $or: [{ userId: id }, { answerUserId: id }] }).toArray();
//         res.json(data);
//     } catch (err) {
//         res.status(500).json({ message: err.message });
//     }
// });

/**
 * @swagger
 * '/api/alerts-answers':
 *  post:
 *      tags:
 *          - Alerts Answers
 *      summary: Ajouter une nouvelle réponse d'alerte
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      properties:
 *                          userID:
 *                              type: number
 *                          content:
 *                              type: string
 *                          title:
 *                              type: string
 *                          linkedAnimalProof:
 *                              type: number
 *      responses:
 *          '201':
 *              description: Réponse d'alerte ajoutée avec succès
 *          '400':
 *              description: Requête invalide
 *          '500':
 *              description: Erreur serveur
 */
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.post('/api/alerts-answers', async (req, res) => {
    const newData = req.body;
    try {
        newData._id = new objectId(); // Générer un nouvel ObjectId et l'assigner à l'objet newData
        const result = await alertsAnswersCollection.insertOne(newData);
        res.status(201).json({ insertedId: newData._id }); // Envoyer l'ID inséré en réponse
    } catch (err) {
        res.status(400).json({ message: err.message });
    }
});

/**
 * @swagger
 * '/api/alerts-answers/{id}':
 *  put:
 *      tags:
 *          - Alerts Answers
 *      summary: Mettre à jour une réponse d'alerte par son ID
 *      parameters:
 *          - name: id
 *            in: path
 *            required: true
 *            description: ID de la réponse d'alerte à mettre à jour
 *            schema:
 *              type: string
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      properties:
 *                          userID:
 *                              type: number  
 *                          content:
 *                              type: string
 *                          title:
 *                              type: string
 *                          linkedAnimalProof:
 *                              type: number
 *      responses:
 *          '200':
 *              description: Réponse d'alerte mise à jour avec succès
 *          '400':
 *              description: Requête invalide
 *          '500':
 *              description: Erreur serveur
 */
app.put('/api/alerts-answers/:id', async (req, res) => {
    const newData = req.body;
    try {
        const result = await alertsAnswersCollection.updateOne(
            { _id: new objectId(req.params.id) },
            { $set: newData }
        );
        res.json(result);
    } catch (err) {
        res.status(400).json({ message: err.message });
    }
});


/**
 * @swagger
 * '/api/alerts-answers/{id}':
 *  delete:
 *      tags:
 *          - Alerts Answers
 *      summary: Supprimer une réponse d'alerte par son ID
 *      parameters:
 *          - name: id
 *            in: path
 *            required: true
 *            description: ID de la réponse d'alerte à supprimer
 *            schema:
 *              type: string
 *      responses:
 *          '200':
 *              description: Réponse d'alerte supprimée avec succès
 *          '404':
 *              description: Réponse d'alerte non trouvée
 *          '500':
 *              description: Erreur serveur
 */
app.delete('/api/alerts-answers/:id', async (req, res) => {
    try {
        const id = new objectId(req.params.id);
        const result = await alertsAnswersCollection.deleteOne({ _id: id });
        if (result.deletedCount === 0) {
            return res.status(404).json({ message: "Aucune réponse d'alerte avec cet ID n'a été trouvée." });
        }
        res.json({ message: "La réponse d'alerte a été supprimée." });
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

module.exports = app;