// Desc: Pictures API
// Date: 12 Février 2024

// Importation des modules
const express = require('express');
const app = express();
const objectId = require('mongodb').ObjectId;
const bodyParser = require('body-parser');

// Importation de la base de données
const dbInstance = require('../database/connect');
const db = dbInstance.getDatabase();
const picturesCollection = db.collection('Pictures');

// Importation et mise en place du CORS pour les requêtes
const cors = require('cors');
app.use(cors());

/**
 * @swagger
 * '/api/pictures':
 *  get:
 *      tags:
 *          - Pictures
 *      summary: Récupérer toutes les images
 *      responses:
 *          '200':
 *              description: Succès
 *          '500':
 *              description: Erreur serveur
 */
app.get('/api/pictures', async (req, res) => {
    try {
        const data = await picturesCollection.find().toArray();
        res.json(data);
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

/**
 * @swagger
 * '/api/pictures/{id}':
 *  get:
 *      tags:
 *          - Pictures
 *      summary: Récupérer une image par son ID
 *      parameters:
 *          - name: id
 *            in: path
 *            required: true
 *            description: ID de l'image à récupérer
 *            schema:
 *              type: string
 *      responses:
 *          '200':
 *              description: Succès
 *          '404':
 *              description: Image non trouvée
 *          '500':
 *              description: Erreur serveur
 */
app.get('/api/pictures/:id', async (req, res) => {
    try {
        var id = new objectId(req.params.id);
        const data = await picturesCollection.findOne({ _id: id });
        res.json(data);
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

/**
 * @swagger
 * '/api/pictures':
 *  post:
 *      tags:
 *          - Pictures
 *      summary: Ajouter une nouvelle image
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      properties:
 *                          pictureID:
 *                              type: integer
 *                          image:
 *                              type: string
 *                          forUsers:
 *                              type: boolean
 *                      example:
 *                          pictureID: 99
 *                          image: "code BASE64"
 *                          forUsers: true
 *      responses:
 *          '201':
 *              description: Image ajoutée avec succès
 *          '400':
 *              description: Requête invalide
 *          '500':
 *              description: Erreur serveur
 */
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.post('/api/pictures', async (req, res) => {
    const newData = req.body;
    try {
        newData._id = new objectId(); // Générer un nouvel ObjectId et l'assigner à l'objet newData
        const result = await picturesCollection.insertOne(newData);
        res.status(201).json({ insertedId: newData._id }); // Envoyer l'ID inséré en réponse
    } catch (err) {
        res.status(400).json({ message: err.message });
    }
});

/**
 * @swagger
 * '/api/pictures/{id}':
 *  put:
 *      tags:
 *          - Pictures
 *      summary: Mettre à jour une image par son ID
 *      parameters:
 *          - name: id
 *            in: path
 *            required: true
 *            description: ID de l'image à mettre à jour
 *            schema:
 *              type: string
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      properties:
 *                          pictureID:
 *                              type: integer
 *                          image:
 *                              type: string
 *                          forUsers:
 *                              type: boolean
 *                      example:
 *                          pictureID: 99
 *                          image: "code BASE64"
 *                          forUsers: true
 *      responses:
 *          '200':
 *              description: Image mise à jour avec succès
 *          '400':
 *              description: Requête invalide
 *          '500':
 *              description: Erreur serveur
 */
app.put('/api/pictures/:id', async (req, res) => {
    const newData = req.body;
    try {
        const result = await picturesCollection.updateOne(
            { _id: new objectId(req.params.id) },
            { $set: newData }
        );
        res.json(result);
    } catch (err) {
        res.status(400).json({ message: err.message });
    }
});

/**
 * @swagger
 * '/api/pictures/{id}':
 *  delete:
 *      tags:
 *          - Pictures
 *      summary: Supprimer une image par son ID
 *      parameters:
 *          - name: id
 *            in: path
 *            required: true
 *            description: ID de l'image à supprimer
 *            schema:
 *              type: string
 *      responses:
 *          '200':
 *              description: Image supprimée avec succès
 *          '404':
 *              description: Image non trouvée
 *          '500':
 *              description: Erreur serveur
 */
app.delete('/api/pictures/:id', async (req, res) => {
    try {
        const id = new objectId(req.params.id);
        const result = await picturesCollection.deleteOne({ _id: id });
        if (result.deletedCount === 0) {
            return res.status(404).json({ message: "Aucune image avec cet ID n'a été trouvée." });
        }
        res.json({ message: "L'image a été supprimée." });
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

module.exports = app;