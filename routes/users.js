// Desc: Users API
// Date: 12 Février 2024

// Importation des modules
const express = require('express');
const app = express();
const objectId = require('mongodb').ObjectId;
const bodyParser = require('body-parser');
const jwt = require('jsonwebtoken');

// Importation de la base de données
const dbInstance = require('../database/connect');
const db = dbInstance.getDatabase();
const usersCollection = db.collection('Users');

const JWT_SECRET = 'secret';

// Importation et mise en place du CORS pour les requêtes
const cors = require('cors');
app.use(cors());


/**
 * @swagger
 * '/api/users':
 *  get:
 *      tags:
 *          - Users
 *      summary: Récupérer tous les utilisateurs
 *      responses:
 *          '200':
 *              description: Succès
 *          '500':
 *              description: Erreur serveur
 */
app.get('/api/users', async (req, res) => {
    try {
        const data = await usersCollection.find().toArray();
        res.json(data);
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});


/**
 * @swagger
 * '/api/users/{id}':
 *  get:
 *      tags:
 *          - Users
 *      summary: Récupérer un utilisateur par son ID
 *      parameters:
 *          - name: id
 *            in: path
 *            required: true
 *            description: ID de l'utilisateur à récupérer
 *            schema:
 *              type: string
 *      responses:
 *          '200':
 *              description: Succès
 *          '404':
 *              description: Utilisateur non trouvée
 *          '500':
 *              description: Erreur serveur
 */
app.get('/api/users/:id', async (req, res) => {
    try {
        var id = new objectId(req.params.id);
        const data = await usersCollection.findOne({ _id: id });
        res.json(data);
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});


/**
 * @swagger
 * '/api/users':
 *  post:
 *      tags:
 *          - Users
 *      summary: Ajouter un nouvel utilisateur
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      properties:
 *                          userID:
 *                              type: number
 *                          email:
 *                              type: string
 *                          password:
 *                              type: string
 *                          telNumber:
 *                              type: string
 *                          name:
 *                              type: string
 *                          surname:
 *                              type: string
 *                          firstname:
 *                              type: string
 *                          adress:
 *                              type: string
 *                          localization:
 *                              type: number
 *                          description:
 *                              type: string
 *                          pictures:
 *                              type: number
 *                      example:
 *                          userID: 1
 *                          email: "example@example.fr"
 *                          password: "password"
 *                          telNumber: "0123456789"
 *                          name: "Doe"
 *                          surname: "John"
 *                          firstname: "John"
 *                          adress: "1 rue de l'exemple"
 *                          localization: 1
 *                          description: "Description de l'utilisateur"
 *                          pictures: 1
 *      responses:
 *          '201':
 *              description: Utilisateur ajoutée avec succès
 *          '400':
 *              description: Requête invalide
 *          '500':
 *              description: Erreur serveur
 */
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.post('/api/users', async (req, res) => {
    const newData = req.body;
    try {
        newData._id = new objectId(); // Générer un nouvel ObjectId et l'assigner à l'objet newData
        const result = await usersCollection.insertOne(newData);
        const user = await usersCollection.findOne({ _id: newData._id });
        res.status(201).json({ user: user }); // Envoyer l'ID inséré en réponse
    } catch (err) {
        res.status(400).json({ message: err });
    }
});


/**
 * @swagger
 * '/api/users/{id}':
 *  put:
 *      tags:
 *          - Users
 *      summary: Mettre à jour un utilisateur par son ID
 *      parameters:
 *          - name: id
 *            in: path
 *            required: true
 *            description: ID de l'utilisateur à mettre à jour
 *            schema:
 *              type: string
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      properties:
 *                          userID:
 *                              type: number
 *                          email:
 *                              type: string
 *                          password:
 *                              type: string
 *                          telNumber:
 *                              type: string
 *                          name:
 *                              type: string
 *                          surname:
 *                              type: string
 *                          firstname:
 *                              type: string
 *                          adress:
 *                              type: string
 *                          localization:
 *                              type: number
 *                          description:
 *                              type: string
 *                          pictures:
 *                              type: number
 *                      example:
 *                          userID: 1
 *                          email: "example@example.fr"
 *                          password: "password"
 *                          telNumber: "0123456789"
 *                          name: "Doe"
 *                          surname: "John"
 *                          firstname: "John"
 *                          adress: "1 rue de l'exemple"
 *                          localization: 1
 *                          description: "Description de l'utilisateur"
 *                          pictures: 1
 *      responses:
 *          '200':
 *              description: Utilisateur mis à jour avec succès
 *          '400':
 *              description: Requête invalide
 *          '500':
 *              description: Erreur serveur
 */
app.put('/api/users/:id', async (req, res) => {
    const newData = req.body;
    try {
        const result = await usersCollection.updateOne(
            { _id: new objectId(req.params.id) },
            { $set: newData }
        );
        res.json(result);
    } catch (err) {
        res.status(400).json({ message: err.message });
    }
});


/**
 * @swagger
 * '/api/users/{id}':
 *  delete:
 *      tags:
 *          - Users
 *      summary: Supprimer un utilisateur par son ID
 *      parameters:
 *          - name: id
 *            in: path
 *            required: true
 *            description: ID de l'utilisateur à supprimer
 *            schema:
 *              type: string
 *      responses:
 *          '200':
 *              description: Utilisateur supprimé avec succès
 *          '404':
 *              description: Utilisateur non trouvé
 *          '500':
 *              description: Erreur serveur
 */
app.delete('/api/users/:id', async (req, res) => {
    try {
        const id = new objectId(req.params.id);
        const result = await usersCollection.deleteOne({ _id: id });
        if (result.deletedCount === 0) {
            return res.status(404).json({ message: "Aucun utilisateur avec cet ID n'a été trouvée." });
        }
        res.json({ message: "L'utilisateur a été supprimé." });
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});


/**
 * @swagger
 * '/login':
 *  post:
 *      tags:
 *          - Authentication
 *      summary: Connectez-vous avec vos identifiants
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      properties:
 *                          email:
 *                              type: string
 *                          password:
 *                              type: string
 *                      example:
 *                          email: example@example.com
 *                          password: password123
 *      responses:
 *          '200':
 *              description: Connexion réussie
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              token:
 *                                  type: string
 *                              user:
 *                                  type: object
 *                                  properties:
 *                                      id:
 *                                          type: string
 *                                      email:
 *                                          type: string
 *          '401':
 *              description: Identifiants incorrects
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              error:
 *                                  type: string
 */
app.post('/login', async (req, res) => {
    const { email, password } = req.body;
    try {
        const user = await usersCollection.findOne({email: email});
        if (!user) {
            throw new Error('Utilisateur non trouvé');
        }

        if (password !== user.password) {
            throw new Error('Mot de passe incorrect');
        }

        const token = jwt.sign({ userID: user._id, userMail: user.email }, JWT_SECRET, { 
            expiresIn: '24h' 
        });

        res.json({
            token,
            user
        });
    } catch (error) {
        res.json({ error: error.message });
    }
});


/**
 * @swagger
 * '/api/users/checkToken':
 *  post:
 *      tags:
 *          - Authentication
 *      summary: Vérifiez la validité d'un token
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      properties:
 *                          token:
 *                              type: string
 *                      example:
 *                          token: [votre_token]
 *      responses:
 *          '200':
 *              description: Token valide
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              _id:
 *                                  type: string
 *                              userID:
 *                                  type: number
 *                              email:
 *                                  type: string
 *                              telNumber:
 *                                  type: string
 *                              name:
 *                                  type: string
 *                              surname:
 *                                  type: string
 *                              firstName:
 *                                  type: string
 *                              address:
 *                                  type: string
 *                              localization:
 *                                  type: boolean
 *                              pictures:
 *                                  type: number
 *          '401':
 *              description: Token invalide
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              error:
 *                                  type: string
 */
app.post('/api/users/checkToken', async (req, res) => {
    const { token } = req.body;
    try {
        const decodedToken = await jwt.verify(token, JWT_SECRET);
        const id = new objectId(decodedToken.userID);
        
        const currentUser = await usersCollection.findOne({_id: id});
        res.json(currentUser);
    } catch (error) {
        res.json({ error: 'Token invalide' });
    }
});


/**
 * @swagger
 * '/api/logout':
 *  post:
 *      tags:
 *          - Authentication
 *      summary: Déconnectez-vous en fournissant un token
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      properties:
 *                          token:
 *                              type: string
 *                      example:
 *                          token: [votre_token]
 *      responses:
 *          '200':
 *              description: Déconnexion réussie
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              message:
 *                                  type: string
 *          '500':
 *              description: Erreur lors de la déconnexion
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              error:
 *                                  type: string
 */
app.post('/api/logout', async (req, res) => {
    const { token } = req.body;
    try {
        const decoded = await jwt.verify(token, JWT_SECRET);
        if (!decoded) {
            throw new Error('Token invalide');
        }
        res.json({ message: 'Déconnexion réussie' });
    } catch (error) {
        res.status(500).json({ error: 'Erreur lors de la déconnexion : ' + error });
    }
});


module.exports = app;