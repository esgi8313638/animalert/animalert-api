// Desc: Index API
// Date: 12 Février 2024

// Importation des modules
const express = require('express');
const app = express();

// Importation et mise en place du CORS pour les requêtes
const cors = require('cors');
app.use(cors());


// GET
app.get('/', async (req, res) => {
    try {
        res.json({
            message: "Bienvenue sur l'API de l'application Animalert"
        });
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});


module.exports = app;