// Importation des modules
var express = require('express');
const http = require('http');
var app = express();
const server = http.createServer(app);
const { Server } = require('socket.io');

// Importation de Swagger
const swaggerJsdoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');

// Configuration de Swagger
const options = {
    definition: {
        openapi: '3.0.0',
        info: {
            title: 'Swagger de l\'API Animalert',
            version: '1.0.0',
            description: 'Documentation pour l\'api de l\'application Animalert',
        },
    },
    apis: ['./routes/*.js'],
};
const specs = swaggerJsdoc(options);

// Importation des routes
const indexRouter = require('./routes/index');
const alertsRouter = require('./routes/alerts');
const alertsAnswersRouter = require('./routes/alertsanswers');
const alertsAnswersPicturesRouter = require('./routes/alertsanswerspictures');
const alertsPicturesRouter = require('./routes/alertspictures');
const animalsRouter = require('./routes/animals');
const belongingsRouter = require('./routes/belonging');
const picturesRouter = require('./routes/pictures');
const usersRouter = require('./routes/users');

// Initialisation des routes
app.use('/', indexRouter);
app.use('/', alertsRouter);
app.use('/', alertsAnswersRouter);
app.use('/', alertsAnswersPicturesRouter);
app.use('/', alertsPicturesRouter);
app.use('/', animalsRouter);
app.use('/', belongingsRouter);
app.use('/', picturesRouter);
app.use('/', usersRouter);
app.use('/docs', swaggerUi.serve, swaggerUi.setup(specs));

const io = new Server(server, {
    cors: {
        origin: "*"
    }
});

io.on('connection', (socket) => {
    console.log('a user connected : ' + socket.id);
    socket.on('disconnect', () => {
        console.log('user disconnected');
    });
    socket.on('chat message', (msg) => {
        console.log('message: ' + msg);
    });
    socket.on('newAnnounce', (announce) => {
        console.log('new announce : ' + announce);
        io.emit('newAnnounce', { announce: announce });
    });
});

const port = 3001;
server.listen(port, () => {
    console.log(`Server is listening on port ${port}`);
});

module.exports = {
    app
};
